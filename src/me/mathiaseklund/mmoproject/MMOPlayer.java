package me.mathiaseklund.mmoproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.Player;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import me.mathiaseklund.mmoproject.utility.GameState;
import me.mathiaseklund.mmoproject.utility.Inventories;

public class MMOPlayer {

	Main main;

	Player player;
	String uuid;
	String userId;
	String characterId;
	GameCharacter character;

	GameState state;

	ArrayList<GameCharacter> characterList = new ArrayList<GameCharacter>();

	// Used to store the selected options in the character creator.
	// ex. <race, human>,<class, warrior>, <name, wkWarrior>
	HashMap<String, String> CreatedCharacter = new HashMap<String, String>();

	public MMOPlayer(Player player) {
		this.main = Main.getMain();
		this.player = player;
		this.uuid = player.getUniqueId().toString();
		this.state = GameState.LOADING;
		main.PlayerList.put(this.uuid, this);
		try {
			GetData();
			GetCharacters();
			OpenCharacterMenu();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void OpenCreateCharacterRaceMenu() {
		Log.info("Attempting to open Create Character menu.");
		SetGameState(GameState.CREATE_CHARACTER_RACE);
		Log.info(player.getOpenInventory().getTitle());
		player.openInventory(Inventories.CreateCharacter_raceInventory);
	}

	public void OpenCreateCharacterClassMenu() {
		Log.info("Attempting to open Create Character menu.");
		SetGameState(GameState.CREATE_CHARACTER_CLASS);
		player.openInventory(Inventories.CreateCharacter_classInventory);
	}

	public void SelectRace(String race) {
		CreatedCharacter.put("race", race);
		OpenCreateCharacterClassMenu();
	}

	public void SelectClass(String c) {
		CreatedCharacter.put("class", c);
		OpenCreateCharacterNameMenu();
	}

	public void OpenCreateCharacterNameMenu() {
		Log.info("Attempting to select name for character.");
		SetGameState(GameState.CREATE_CHARACTER_NAME);
		player.closeInventory();
		// TODO
	}

	public void LoadCharacterList() {
		//TODO
		// for (GameCharacter character : characterList) {
		// Log.info(character.getName());
		// }
	}

	public String GetUserId() {
		return this.userId;
	}

	public ArrayList<GameCharacter> GetCharacterList() {
		return this.characterList;
	}

	public void OpenCharacterMenu() {
		Log.info("Attempting to open the character menu GUI");
		CreatedCharacter.clear();
		if (!(GetGameState() == GameState.CHOOSE_CHARACTER)) {
			Log.info("Setting GameState to CHOOSE");
			SetGameState(GameState.CHOOSE_CHARACTER);
		}
		player.openInventory(Inventories.ChooseCharacterInventory);

	}

	public GameState GetGameState() {
		return this.state;
	}

	private void SetGameState(GameState state) {
		this.state = state;
	}

	private void GetData() throws IOException {
		main.log.info("Attempting to fetch " + this.player.getName() + "'s account data.");

		StringBuilder result = new StringBuilder();
		URL url = new URL(main.apiURL.toString() + "/playerdata/" + this.uuid);
		main.log.info("Sending GET request to: " + url);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String line;
		while ((line = br.readLine()) != null) {
			result.append(line);
		}
		br.close();
		Log.info(result);

		JsonObject res = new JsonParser().parse(result.toString()).getAsJsonObject();
		SetUserId(res.get("_id").getAsString());
	}

	private void GetCharacters() {
		main.log.info("Attempting to fetch " + this.player.getName() + "'s character data.");

		StringBuilder result = new StringBuilder();
		URL url;
		try {
			url = new URL(main.apiURL.toString() + "/characters/" + this.uuid);

			main.log.info("Sending GET request to: " + url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String line;
				while ((line = br.readLine()) != null) {
					result.append(line);
				}
				br.close();
				Log.info("result: " + result);

				JsonObject res = new JsonParser().parse(result.toString()).getAsJsonObject();
				Log.info(res.getAsString());
			} else if (responseCode == 404) {
				Log.error("Unable to find any characters.");
			} else {
				Log.error("There was an unexpected error loading characters. ResponseCode: " + responseCode);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void SetUserId(String userId) {
		this.userId = userId;
	}
}
