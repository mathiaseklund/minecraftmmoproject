package me.mathiaseklund.mmoproject.utility;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Inventories {

	public static Inventory ChooseCharacterInventory = Bukkit.createInventory(null, (9 * 6), "Choose Character");
	public static Inventory CreateCharacter_raceInventory = Bukkit.createInventory(null, (9 * 1),
			"Create Character - Select Race");
	public static Inventory CreateCharacter_classInventory = Bukkit.createInventory(null, (9 * 1),
			"Create Character - Select Class");
	public static Inventory CreateCharacter_overviewInventory = Bukkit.createInventory(null, (9 * 1),
			"Create Character - Overview");

	public static void CreateChooseCharacterInventory() {
		ItemStack createCharacterItemStack = new ItemStack(Material.GREEN_WOOL, 1);
		ItemMeta createCharacterItemMeta = createCharacterItemStack.getItemMeta();
		createCharacterItemMeta.setDisplayName("Create New Character");
		createCharacterItemStack.setItemMeta(createCharacterItemMeta);

		ChooseCharacterInventory.setItem(45, createCharacterItemStack);
	}

	public static void CreateCreateCharacterRaceInventory() {
		ItemStack raceHumanItemStack = new ItemStack(Material.PLAYER_HEAD, 1);
		ItemMeta raceHumanItemMeta = raceHumanItemStack.getItemMeta();
		raceHumanItemMeta.setDisplayName("Human");
		raceHumanItemStack.setItemMeta(raceHumanItemMeta);

		ItemStack cancelItemStack = new ItemStack(Material.RED_WOOL, 1);
		ItemMeta cancelItemMeta = cancelItemStack.getItemMeta();
		cancelItemMeta.setDisplayName("Cancel Character Creation");
		cancelItemStack.setItemMeta(cancelItemMeta);

		CreateCharacter_raceInventory.setItem(CreateCharacter_raceInventory.getSize() - 1, cancelItemStack);
		CreateCharacter_raceInventory.addItem(raceHumanItemStack);
	}

	public static void CreateCreateCharacterClassInventory() {
		ItemStack classWarriorItemStack = new ItemStack(Material.IRON_SWORD, 1);
		ItemMeta classWarriorItemMeta = classWarriorItemStack.getItemMeta();
		classWarriorItemMeta.setDisplayName("Warrior");
		classWarriorItemStack.setItemMeta(classWarriorItemMeta);

		ItemStack cancelItemStack = new ItemStack(Material.RED_WOOL, 1);
		ItemMeta cancelItemMeta = cancelItemStack.getItemMeta();
		cancelItemMeta.setDisplayName("Cancel Character Creation");
		cancelItemStack.setItemMeta(cancelItemMeta);

		CreateCharacter_classInventory.setItem(CreateCharacter_classInventory.getSize() - 1, cancelItemStack);
		CreateCharacter_classInventory.addItem(classWarriorItemStack);
	}
}
