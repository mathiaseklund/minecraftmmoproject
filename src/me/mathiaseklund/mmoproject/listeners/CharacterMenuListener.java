package me.mathiaseklund.mmoproject.listeners;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.mathiaseklund.mmoproject.MMOPlayer;
import me.mathiaseklund.mmoproject.Main;
import me.mathiaseklund.mmoproject.utility.GameState;

public class CharacterMenuListener implements Listener {

	Main main = Main.getMain();

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		player.closeInventory();
		new MMOPlayer(player);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		main.PlayerList.remove(player.getUniqueId().toString());
	}

	@EventHandler
	public void onInventoryOpen(InventoryOpenEvent event) {
		MMOPlayer player = main.PlayerList.get(event.getPlayer().getUniqueId().toString());
		if (player.GetGameState() == GameState.CHOOSE_CHARACTER) {
			// Load users characters in Inventory Window

			if (player.GetCharacterList().isEmpty()) {
				Log.warn(player.GetUserId() + "'s Character List is Empty");
			} else {
				player.LoadCharacterList();
			}
		} else if (player.GetGameState() == GameState.CREATE_CHARACTER_RACE) {
			// Opening the CreateCharacter Inventory
		} else {
			// Misc
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		MMOPlayer player = main.PlayerList.get(event.getPlayer().getUniqueId().toString());
		if (player != null) {
			if (player.GetGameState() == GameState.CHOOSE_CHARACTER) {
				if (event.getInventory().getTitle().contains("Choose Character")) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
						public void run() {
							player.OpenCharacterMenu();
						}
					}, 1);
				}
			} else if (player.GetGameState() == GameState.CREATE_CHARACTER_RACE) {
				if (event.getInventory().getTitle().contains("Race")) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
						public void run() {
							player.OpenCreateCharacterRaceMenu();
						}
					}, 1);
				}
			} else if (player.GetGameState() == GameState.CREATE_CHARACTER_CLASS) {
				if (event.getInventory().getTitle().contains("Class")) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
						public void run() {
							player.OpenCreateCharacterClassMenu();
						}
					}, 1);
				}
			}
		}
	}

	@EventHandler
	public void onClickInventory(InventoryClickEvent event) {
		MMOPlayer player = main.PlayerList.get(event.getWhoClicked().getUniqueId().toString());
		String title = event.getInventory().getTitle();
		Log.info("Title: " + title);
		if (player.GetGameState() == GameState.CHOOSE_CHARACTER) {
			// Clicked in Choose Character Inventory
			if (event.getCurrentItem() != null) {
				Log.info("Clicked on Something");
				ItemStack is = event.getCurrentItem();
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.getDisplayName().equalsIgnoreCase("Create New Character")) {
						// Wants to create a new character.
						Log.info("Open Create Character menu");
						player.OpenCreateCharacterRaceMenu();
					} else {
						// Clicked on an existing character.
						Log.info("Start playing with selected Character.");
					}
				}
			} else {
				Log.info("Clicked on Nothing.");
			}
			event.setCancelled(true);
		} else if (player.GetGameState() == GameState.CREATE_CHARACTER_RACE) {
			// Clicked in Create Character RACE Inventory

			ItemStack is = event.getCurrentItem();
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.getDisplayName().contains("Human")) {
						// Selected Human Race
						Log.info("Selected Human Race");
						player.SelectRace("human");
					} else if (im.getDisplayName().contains("Cancel")) {
						// Cancel creation
						Log.info("Cancelling Character Creation");
						player.OpenCharacterMenu();
					} else {
						Log.info("Clicked on Something");
					}
				} else {
					Log.info("Clicked on Something");
				}
			} else {
				Log.info("Clicked on Nothing");
			}

			event.setCancelled(true);
		} else if (player.GetGameState() == GameState.CREATE_CHARACTER_CLASS) {
			// Clicked in Create Character CLASS Inventory

			ItemStack is = event.getCurrentItem();
			if (is != null) {
				if (is.hasItemMeta()) {
					ItemMeta im = is.getItemMeta();
					if (im.getDisplayName().contains("Warrior")) {
						// Selected Human Race
						Log.info("Selected Warrior Class");
						player.SelectClass("warrior");
					} else if (im.getDisplayName().contains("Cancel")) {
						// Cancel creation
						Log.info("Cancelling Character Creation");
						player.OpenCharacterMenu();
					} else {
						Log.info("Clicked on Something");
					}
				} else {
					Log.info("Clicked on Something");
				}
			} else {
				Log.info("Clicked on Nothing");
			}

			event.setCancelled(true);
		}

	}
}
