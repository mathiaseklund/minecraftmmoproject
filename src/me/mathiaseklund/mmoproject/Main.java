package me.mathiaseklund.mmoproject;

import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.mmoproject.listeners.CharacterMenuListener;
import me.mathiaseklund.mmoproject.utility.Inventories;

public class Main extends JavaPlugin {

	public Logger log;

	static Main main;

	public HashMap<String, MMOPlayer> PlayerList = new HashMap<String, MMOPlayer>();
	public String apiURL;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;
		log = Logger.getLogger("Minecraft");
		getServer().getPluginManager().registerEvents(new CharacterMenuListener(), this);
		saveDefaultConfig();
		CreateDefaultInventories();
		apiURL = getConfig().getString("apiurl");
		log.info("Fetched URL String: " + apiURL);

		ReLoginPlayers();
	}

	public void onDisable() {

		System.out.println("Disabling MMOProject Plugin");
	}

	private void CreateDefaultInventories() {

		Inventories.CreateChooseCharacterInventory();
		Inventories.CreateCreateCharacterClassInventory();
		Inventories.CreateCreateCharacterRaceInventory();
	}

	private void ReLoginPlayers() {
		for (Player player : getServer().getOnlinePlayers()) {
			getServer().getPluginManager()
					.callEvent(new PlayerJoinEvent(player, player.getName() + " Rejoined the Server"));
		}
	}
}
